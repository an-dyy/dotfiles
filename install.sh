#!/bin/sh

if [ $EUID -ne 0 ]; then
    continue
else
    echo "Don't run with sudo"
    exit 1
fi

BASEDIR=$(dirname "$BASH_SOURCE")
CDIR=$HOME/.config/

sudo -v

read -p "Press any key to start..."
sleep 0.5

echo "Installing some packages"
sudo pacman -S nitrogen alacritty neofetch --noconfirm

echo "Installing zsh and setting up"
sudo pacman -S zsh --noconfirm
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" &
cp $BASEDIR/antigen.zsh $CDIR
cp $BASEDIR/.zshrc $HOME
source $HOME/.zshrc

echo "Installing xorg"
sudo pacman -S xorg --noconfirm

echo "Installing lightdm"
sudo pacman -S lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings --noconfirm

echo "Installing bspwm, sxhkd"
sudo pacman -S bspwm sxhkd --noconfirm

read -p "Press any key to continue..."
clear

echo "Backing up current configs"
mkdir -p $HOME/Desktop/backup
cp $HOME/.config/ $HOME/Desktop/backup

echo "Setting up sxhkd and bspwm..."
mkdir -p $CDIR/bspwm
mkdir -p $CDIR/sxhkd

install -Dm755 $BASEDIR/.config/bspwm/bspwmrc $CDIR/bspwm/bspwmrc
install -Dm755 $BASEDIR/.config/sxhkd/sxhkdrc $CDIR/sxhkd/sxhkdrc

echo "Setting up alacritty config..."
cp $BASEDIR/.config/alacritty $CDIR -r

echo "Setting up neofetch config..."
cp $BASEDIR/.config/neofetch $CDIR -r

clear

echo "Select wallpaper"
nitrogen $BASEDIR/Wallpapers &

if ! [ command -v yay &> /dev/null ]; then
    sudo pacman -S --needed git base-devel --noconfirm
    git clone https://aur.archlinux.org/yay.git && cd yay
    makepkg -si --noconfirm
    cd $BASEDIR
else
    continue
fi

echo "Installing nvim"
sudo pacman -S neovim --noconfirm
cp $BASEDIR/.config/nvim $CDIR -r

echo "Installing rofi"
yay -S rofi --noconfirm
yay -S rofi-power-menu --noconfirm
cp $BASEDIR/.config/rofi $CDIR -r

echo "Installing picom"
yay -S picom-jonaburg-git --noconfirm
cp $BASEDIR/.config/picom $CDIR -r

echo "Installing polybar"
yay -S polybar --noconfirm
cp $BASEDIR/.config/polybar $CDIR -r

echo "Installing colorscript"
yay -S shell-color-scripts --noconfirm

sudo systemctl enable lightdm
read -p "Rebooting, press any key to confirm..."
sudo reboot
